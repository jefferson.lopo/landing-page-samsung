//slick mobile
arrayimage = new Array("./img/pass1.png", "./img/pass2.png", "./img/pass3.png")

function comeco() {
  document.getElementById('imgpass').src = arrayimage[0]
  document.form.texto.value = "0"
}

function mais() {
  document.form.texto.value = Math.floor(1 + 1 - 2 + (document.form.texto.value) * 1 + 1)
  if (document.form.texto.value > 2) { document.form.texto.value = 0 }
  document.getElementById('imgpass').style.display = "none";
}

function menos() {
  document.form.texto.value = Math.floor(1 + 1 - 2 + (document.form.texto.value) * 1 - 1)
  if (document.form.texto.value < 0) { document.form.texto.value = 2 }
  document.getElementById('imgpass').style.display = "none";
}

function regular() {
  document.getElementById('imgpass').src = arrayimage[document.form.texto.value];
  setTimeout("regular()", 100);
  document.getElementById('imgpass').style.display = "block";

  var validar_etapa = document.form.texto.value
  if (validar_etapa == 0) {
    document.getElementById("img-prev").style.display = "none";
  }
  else {
    document.getElementById("img-prev").style.display = "block";
  }
  if (validar_etapa == 2) {
    document.getElementById("img-next").style.display = "none";
  }
  else {
    document.getElementById("img-next").style.display = "block";
  }
}

comeco();
regular();



//filtro
//lista de informacoes 

//estado
var estate = [
  {
    nome: "AC",
  },
  {
    nome: "AL",
  },
  {
    nome: "AM",
  },
  {
    nome: "BA",
  },
  {
    nome: "CE",
  },
  {
    nome: "DF",
  },
  {
    nome: "ES",
  },
  {
    nome: "GO",
  },
  {
    nome: "MA",
  },
  {
    nome: "MG",
  },
  {
    nome: "MS",
  },
  {
    nome: "MT",
  },
  {
    nome: "PA",
  },
  {
    nome: "PB",
  },
  {
    nome: "PE",
  },
  {
    nome: "PI",
  },
  {
    nome: "PR",
  },
  {
    nome: "RJ",
  },
  {
    nome: "RN",
  },
  {
    nome: "RO",
  },
  {
    nome: "RS",
  },
  {
    nome: "SC",
  },
  {
    nome: "SE",
  },
  {
    nome: "SP",
  },
  {
    nome: "TO",
  }
]

//cidade
var city = [
  {
    nome: "Rio Branco",
    estado: "AC"
  },
  {
    nome: "Arapiraca",
    estado: "AL"
  },
  {
    nome: "Maceio",
    estado: "AL"
  },
  {
    nome: "Manaus",
    estado: "AM"
  },
  {
    nome: "Feira de Santana",
    estado: "BA"
  },
  {
    nome: "Itabuna",
    estado: "BA"
  },
  {
    nome: "Salvador",
    estado: "BA"
  },
  {
    nome: "Vitoria da Conquista",
    estado: "BA"
  },
  {
    nome: "Fortaleza",
    estado: "CE"
  },
  {
    nome: "Juazeiro do Norte",
    estado: "CE"
  },
  {
    nome: "Maracanau",
    estado: "CE"
  },
  {
    nome: "Sobral",
    estado: "CE"
  },
  {
    nome: "Brasília",
    estado: "DF"
  },
  {
    nome: "Cariacica",
    estado: "ES"
  },
  {
    nome: "Serra",
    estado: "ES"
  },
  {
    nome: "Vila Velha",
    estado: "ES"
  },
  {
    nome: "Vitória",
    estado: "ES"
  },
  {
    nome: "Anápolis",
    estado: "GO"
  },
  {
    nome: "Aparecida de Goiânia",
    estado: "GO"
  },
  {
    nome: "Goiânia",
    estado: "GO"
  },
  {
    nome: "Rio Verde",
    estado: "GO"
  },
  {
    nome: "Val Paraiso",
    estado: "GO"
  },
  {
    nome: "Imperatriz",
    estado: "MA"
  },
  {
    nome: "São Luis",
    estado: "MA"
  },
  {
    nome: "São José de Ribamar",
    estado: "MA"
  },
  {
    nome: "Belo Horizonte",
    estado: "MG"
  },
  {
    nome: "Contagem",
    estado: "MG"
  },
  {
    nome: "Ipatinga",
    estado: "MG"
  },
  {
    nome: "Juiz de Fora",
    estado: "MG"
  },
  {
    nome: "Montes Claros",
    estado: "MG"
  },
  {
    nome: "Sete Lagoas",
    estado: "MG"
  },
  {
    nome: "Uberlandia",
    estado: "MG"
  },
  {
    nome: "Uberaba",
    estado: "MG"
  },
  {
    nome: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Rondonópolis",
    estado: "MT"
  },
  {
    nome: "Sorriso",
    estado: "MT"
  },
  {
    nome: "Várzea Grande",
    estado: "MT"
  },
  {
    nome: "Campo Grande",
    estado: "MS"
  },
  {
    nome: "Ananindeua",
    estado: "PA"
  },
  {
    nome: "Belém",
    estado: "PA"
  },
  {
    nome: "Marabá",
    estado: "PA"
  },
  {
    nome: "Parauapebas",
    estado: "PA"
  },
  {
    nome: "João Pessoa",
    estado: "PB"
  },
  {
    nome: "Campina grande",
    estado: "PB"
  },
  {
    nome: "Caruaru",
    estado: "PE"
  },
  {
    nome: "Jaboatão dos Guararapes",
    estado: "PE"
  },
  {
    nome: "Olinda",
    estado: "PE"
  },
  {
    nome: "Recife",
    estado: "PE"
  },
  {
    nome: "Teresina",
    estado: "PI"
  },
  {
    nome: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Londrina",
    estado: "PR"
  },
  {
    nome: "Maringá",
    estado: "PR"
  },
  {
    nome: "Ponta Grossa",
    estado: "PR"
  },
  {
    nome: "São José dos Pinhais",
    estado: "PR"
  },
  {
    nome: "Campo dos Goytacazes",
    estado: "RJ"
  },
  {
    nome: "Cabo Frio",
    estado: "RJ"
  },
  {
    nome: "Duque de Caxias",
    estado: "RJ"
  },
  {
    nome: "Itaguai",
    estado: "RJ"
  },
  {
    nome: "Macaé",
    estado: "RJ"
  },
  {
    nome: "Nilópolis",
    estado: "RJ"
  },
  {
    nome: "Niterói",
    estado: "RJ"
  },
  {
    nome: "Nova Iguaçu",
    estado: "RJ"
  },
  {
    nome: "Nova Friburgo",
    estado: "RJ"
  },
  {
    nome: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "São João do Meriti",
    estado: "RJ"
  },
  {
    nome: "São Gonçalo",
    estado: "RJ"
  },
  {
    nome: "Volta Redonda",
    estado: "RJ"
  },
  {
    nome: "Teresópolis",
    estado: "RJ"
  },
  {
    nome: "Natal",
    estado: "RN"
  },
  {
    nome: "Porto Velho",
    estado: "RO"
  },
  {
    nome: "Bento Gonçalves",
    estado: "RS"
  },
  {
    nome: "Novo Hamburgo",
    estado: "RS"
  },
  {
    nome: "Passo Fundo",
    estado: "RS"
  },
  {
    nome: "Pelotas",
    estado: "RS"
  },
  {
    nome: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Santa Maria",
    estado: "RS"
  },
  {
    nome: "São Leoplodo",
    estado: "RS"
  },
  {
    nome: "Caxias do Sul",
    estado: "RS"
  },
  {
    nome: "Canoas",
    estado: "RS"
  },
  {
    nome: "Blumenau",
    estado: "SC"
  },
  {
    nome: "Camboriu",
    estado: "SC"
  },
  {
    nome: "Chapecó",
    estado: "SC"
  },
  {
    nome: "Criciuma",
    estado: "SC"
  },
  {
    nome: "Florianópolis",
    estado: "SC"
  },
  {
    nome: "Itajai",
    estado: "SC"
  },
  {
    nome: "Jaraguá do Sul",
    estado: "SC"
  },
  {
    nome: "Joinville",
    estado: "SC"
  },
  {
    nome: "Lages",
    estado: "SC"
  },
  {
    nome: "Palhoça",
    estado: "SC"
  },
  {
    nome: "São José",
    estado: "SC"
  },
  {
    nome: "Tubarao",
    estado: "SC"
  },
  {
    nome: "Aracaju",
    estado: "SE"
  },
  {
    nome: "Nossa Senhora do Socorro",
    estado: "SE"
  },
  {
    nome: "Araçatuba",
    estado: "SP"
  },
  {
    nome: "Araraquara",
    estado: "SP"
  },
  {
    nome: "Barueri",
    estado: "SP"
  },
  {
    nome: "Bauru",
    estado: "SP"
  },
  {
    nome: "Cotia",
    estado: "SP"
  },
  {
    nome: "Campinas",
    estado: "SP"
  },
  {
    nome: "Diadema",
    estado: "SP"
  },
  {
    nome: "Franca",
    estado: "SP"
  },
  {
    nome: "Diadema",
    estado: "SP"
  },
  {
    nome: "Guaratingueta",
    estado: "SP"
  },
  {
    nome: "Guarulhos",
    estado: "SP"
  },
  {
    nome: "Itu",
    estado: "SP"
  },
  {
    nome: "Itaquaquecetuba",
    estado: "SP"
  },
  {
    nome: "Indaiatuba",
    estado: "SP"
  },
  {
    nome: "Jundiai",
    estado: "SP"
  },
  {
    nome: "Limeira",
    estado: "SP"
  },
  {
    nome: "Maua",
    estado: "SP"
  },
  {
    nome: "Marilia",
    estado: "SP"
  },
  {
    nome: "Mogi das Cruzes",
    estado: "SP"
  },
  {
    nome: "Mogi Guaçú",
    estado: "SP"
  },
  {
    nome: "Osasco",
    estado: "SP"
  },
  {
    nome: "Praia Grande",
    estado: "SP"
  },
  {
    nome: "Piracicaba",
    estado: "SP"
  },
  {
    nome: "Praia Grande",
    estado: "SP"
  },
  {
    nome: "Presidente Prudente",
    estado: "SP"
  },
  {
    nome: "Ribeirão Preto",
    estado: "SP"
  },
  {
    nome: "Rio Claro",
    estado: "SP"
  },
  {
    nome: "Sorocaba",
    estado: "SP"
  },
  {
    nome: "São Carlos",
    estado: "SP"
  },
  {
    nome: "São José do Rio Preto",
    estado: "SP"
  },
  {
    nome: "Santa Barbara do Oeste",
    estado: "SP"
  },
  {
    nome: "Santo André",
    estado: "SP"
  },
  {
    nome: "São Vicente",
    estado: "SP"
  },
  {
    nome: "Santos",
    estado: "SP"
  },
  {
    nome: "São José dos Campos",
    estado: "SP"
  },
  {
    nome: "São Caetano",
    estado: "SP"
  },
  {
    nome: "Suzano",
    estado: "SP"
  },
  {
    nome: "São Bernardo",
    estado: "SP"
  },
  {
    nome: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Taboão da Serra",
    estado: "SP"
  },
  {
    nome: "Taubate",
    estado: "SP"
  },
  {
    nome: "Votorantim",
    estado: "SP"
  },
  {
    nome: "Palmas",
    estado: "TO"
  }
]

//loja
var store = [
  {
    nome: "Via Verde Shopping",
    endereco: "Est Da Floresta, 2320 - Loja 20/21/22",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio Branco",
    estado: "AC"
  },
  {
    nome: "Arapiraca Garden Shopping",
    endereco: "R Jose Leite Bezerra, S/N Loja 264",
    contato: "https://api.whatsapp.com/send?phone=5583991823631&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Arapiraca",
    estado: "AL"
  },
  {
    nome: "Maceió Shopping",
    endereco: "Av Comendador Gustavo Paiva, 2990 -Loja 296",
    contato: "https://api.whatsapp.com/send?phone=5583991823631&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Maceio",
    estado: "AL"
  },
  {
    nome: "Shopping Pátio Maceió",
    endereco: "Avenida Menino Marcelo, 3800 - Quiosque 018",
    contato: "https://api.whatsapp.com/send?phone=5583991823631&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Maceio",
    estado: "AL"
  },
  {
    nome: "Amazonas Shopping",
    endereco: "Av. Djalma Batista, 482  - Loja 143",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Horá de funcionamento: 09h - 18h",
    cidade: "Manaus",
    estado: "AM"
  },
  {
    nome: "Shopping Ponta Negra",
    endereco: "Av Cel Teixeira, 5705 -1º Piso - Loja 68",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Manaus",
    estado: "AM"
  },
  {
    nome: "Manauara Shopping",
    endereco: "Av Mario Ypiranga, 1300 - Loja 67/68",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Manaus",
    estado: "AM"
  },
  {
    nome: "Sumaúma Park Shopping",
    endereco: "Av. Noel Nutel, 1762 - Piso Arara - Loja 2077/2078",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Manaus",
    estado: "AM"
  },
  {
    nome: "Shopping Grande Circular",
    endereco: "Av. Autaz Mirim, 6100 - Quiosque 113",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Manaus",
    estado: "AM"
  },
  {
    nome: "Boulevard Shopping",
    endereco: "Av. Governador João Durval Carneiro, 3665 - Piso Térreo - Loja 196",
    contato: "https://api.whatsapp.com/send?phone=557536038007&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Feira de Santana",
    estado: "BA"
  },
  {
    nome: "Shopping Jequitiba",
    endereco: "Av Aziz Maron, S/N - 1º Piso - Loja 26A",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Itabuna",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Shopping da Bahia Piso 3",
    endereco: "Av.Tancredo Neves, 148 - 3º Piso - Loja 1A3",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Salvador",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Shopping Paralela",
    endereco: "Av Luiz Viana Filho, 8544 - 1º Piso - Loja 106",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Salvador",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Salvador Norte Shopping",
    endereco: "Rod Ba-526, 305 - 2º Piso - Loja 2004",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Salvador",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Shopping Barra Salvador",
    endereco: "Av Centenario, 2992 - 1º Piso - Loja 149",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Salvador",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Shopping Bela Vista",
    endereco: "Almeida Euvaldo Luz, 92 - Piso L1 - Quiosque 19",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Salvador",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Shopping da Bahia",
    endereco: "Av. Tancredoneves, 148 - 1º Piso - Loja 09/10",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Salvador",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Salvador Shopping",
    endereco: "Av. Tancredo Neves, 3133 - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Salvador",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Shopping Conquista Sul",
    endereco: "Av. Juracy Magalhaes, 3340 - Piso Térreo - Loja 47/48",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Vitoria da Conquista",
    horario: "Loja temporariamente fechada",
    estado: "BA"
  },
  {
    nome: "Shopping RioMar Fortaleza",
    endereco: "R Lauro Nogueira, 1355 - Piso L1 Loja 1140",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Fortaleza",
    horario: "Loja temporariamente fechada",
    estado: "CE"
  },
  {
    nome: "Shopping Iguatemi Fortaleza",
    endereco: "Av. Washington Soares, 85 - Piso L1 Loja 603",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Fortaleza",
    horario: "Loja temporariamente fechada",
    estado: "CE"
  },
  {
    nome: "Via Sul Shopping",
    endereco: "Av. Washington Soares, 4335 - Térreo Loja 107",
    contato: "https://api.whatsapp.com/send?phone=558534729110&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Fortaleza",
    horario: "Loja temporariamente fechada",
    estado: "CE"
  },
  {
    nome: "North Shopping Joquei",
    endereco: "Av. Lineu Machado, 419 - Piso L1 - Loja 1031/1032",
    contato: "https://api.whatsapp.com/send?phone=558534729110&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Fortaleza",
    horario: "Loja temporariamente fechada",
    estado: "CE"
  },
  {
    nome: "RioMar Kennedy",
    endereco: "Av. Sarmento Herminio Sampaio, 3100 - Piso L2",
    contato: "https://api.whatsapp.com/send?phone=558534729110&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Fortaleza",
    horario: "Loja temporariamente fechada",
    estado: "CE"
  },
  {
    nome: "Grand Shopping",
    endereco: "Av. Frei Cirilo, 3840 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=558534729110&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    cidade: "Fortaleza",
    horario: "Loja temporariamente fechada",
    estado: "CE"
  },
  {
    nome: "North Shopping Fortaleza",
    endereco: "Av. Bezerra De Menezes, 2450 - 1º Piso Quiosque 27",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Fortaleza",
    estado: "CE"
  },
  {
    nome: "Shopping Del Paseo",
    endereco: "Av. Santos Dumont, 3131 - Piso L2 - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Fortaleza",
    estado: "CE"
  },
  {
    nome: "North Shopping Fortaleza 2",
    endereco: "Av. Bezerra de Menezes, 2450 - 1º Piso Loja 442/443",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Fortaleza",
    estado: "CE"
  },
  {
    nome: "Shopping Cariri Garden",
    endereco: "Av Padre Cicero, 2555 - Piso L1 Loja 206/207",
    contato: "https://api.whatsapp.com/send?phone=5583991823631&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Juazeiro do Norte",
    estado: "CE"
  },
  {
    nome: "North Shopping Maracanau",
    endereco: "Av. Senador Carlos Jereissati, 100 - 2º Piso Loja 240",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Maracanau",
    estado: "CE"
  },
  {
    nome: "Sobral Shopping",
    endereco: "Av. Monsenhor Aloísio Pinto, 300 - Piso Térreo - Quiosque 05",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Sobral",
    estado: "CE"
  },
  {
    nome: "Park Shopping Brasília",
    endereco: "St De Areas Isoladas/Sudoeste Area, 6580 - Loja 283-284",
    contato: "https://api.whatsapp.com/send?phone=556130378014&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Brasília Shopping",
    endereco: "St Comercial Norte Quadra, 05 - Loja 94S/98S Bloco A",
    contato: "https://api.whatsapp.com/send?phone=556130378014&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Shopping Conjunto Nacional",
    endereco: "Outros Sdn Cnb Conjunto A, S/N - 2º Piso - Loja 2072",
    contato: "https://api.whatsapp.com/send?phone=556130378014&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "JK Shopping",
    endereco: "Q Qnm 34 Area Especial, 01 - 2º Piso - Loja 314",
    contato: "https://api.whatsapp.com/send?phone=556130378014&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Taguatinga Shopping",
    endereco: "Q Qs 01 Rua 210 Lote 40 Salao Comercial - 2º Piso - Loja 3065",
    contato: "https://api.whatsapp.com/send?phone=556130378014&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Patio Brasil Shopping",
    endereco: "St Comercial Sul Quadra 07 Bloco A S/N - Piso Térreo - Loja 01T",
    contato: "https://api.whatsapp.com/send?phone=556130378014&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Terraço Shopping",
    endereco: "SHC /AO/S EA 2/8 Lote 05 - 1º Piso - Loja 286",
    contato: "https://api.whatsapp.com/send?phone=556130378014&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Conjunto Nacional Brasília",
    endereco: "St De Diversoes Norte Conjunto A, S/N - 1º Piso - Loja 2072",
    contato: "https://api.whatsapp.com/send?phone=556130378014&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Shopping Iguatemi Brasília",
    endereco: "St Shin Ca 4 Lote A, S/N - Piso Térreo - Loja 40",
    contato: "https://api.whatsapp.com/send?phone=556130347009&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Boulevard Shopping Brasília",
    endereco: "St Terminal Norte Conjunto J, S/N - Quiosque 23",
    contato: "https://api.whatsapp.com/send?phone=556130347009&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Brasília",
    estado: "DF"
  },
  {
    nome: "Shopping Moxuara",
    endereco: "Avenida Mario Gurgel, 5353 - Piso L3 - Loja 306A",
    contato: "https://api.whatsapp.com/send?phone=552733867642&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Cariacica",
    estado: "ES"
  },
  {
    nome: "Shopping Mestre Álvaro",
    endereco: "Av. João Palácio, 300 - Quiosque 106A",
    contato: "https://api.whatsapp.com/send?phone=552733867642&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Serra",
    estado: "ES"
  },
  {
    nome: "Shopping Praia da Costa",
    endereco: "Av. Dr. Olívio Lira, 353 - Quiosque 107A",
    contato: "https://api.whatsapp.com/send?phone=552733867642&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Vila Velha",
    estado: "ES"
  },
  {
    nome: "Shopping Vila Velha",
    endereco: "Rua Luciano das Neves, 2418 - 1º Piso - Quiosque 1009",
    contato: "https://api.whatsapp.com/send?phone=552733867642&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Vila Velha",
    estado: "ES"
  },
  {
    nome: "Boulevard Shopping Vila Velha",
    endereco: "Rod do Sol, 5000 - 1º Piso - Quiosque 122",
    contato: "https://api.whatsapp.com/send?phone=552733867642&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Vila Velha",
    estado: "ES"
  },
  {
    nome: "Shopping Vitória",
    endereco: "Av. Américo Buaiz, 200 - Loja 604",
    contato: "https://api.whatsapp.com/send?phone=552733867642&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Vitória",
    estado: "ES"
  },
  {
    nome: "Shopping Vitória Kiosk",
    endereco: "Av. Américo Buais, 200 - Quiosque S103",
    contato: "https://api.whatsapp.com/send?phone=552733867642&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Vitória",
    estado: "ES"
  },
  {
    nome: "Brasil Park Shopping",
    endereco: "Av. Brasil, 505 Piso 1°QE.15 - 1° Piso - Loja 505",
    contato: "https://api.whatsapp.com/send?phone=5562996031718&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Anápolis",
    estado: "GO"
  },
  {
    nome: "Shopping Buriti",
    endereco: "Av. Rio Verde, Vila São Tomaz - 1º Piso - Loja 177",
    contato: "https://api.whatsapp.com/send?phone=5562996710732&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Aparecida de Goiânia",
    estado: "GO"
  },
  {
    nome: "Flamboyant Shopping Center",
    endereco: "Av. Deputado Jamel Cecílio, 3300 - 2º Piso Loja S258",
    contato: "https://api.whatsapp.com/send?phone=5562996710732&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Goiânia",
    estado: "GO"
  },
  {
    nome: "Passeio das Aguas Shopping",
    endereco: "Av Perimetral Norte, 8303 - 1º Piso Loja 113/114",
    contato: "https://api.whatsapp.com/send?phone=5562996710732&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Goiânia",
    estado: "GO"
  },
  {
    nome: "Portal Shopping",
    endereco: "Av Anhanguera, 14.404 Quadra Area Lote 39 - Piso Térreo - Loja 290",
    contato: "https://api.whatsapp.com/send?phone=5562996710732&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Goiânia",
    estado: "GO"
  },
  {
    nome: "Shopping Araguaia",
    endereco: "Rua 44, 399 St Central - Piso Térreo",
    contato: "https://api.whatsapp.com/send?phone=556239245165&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Goiânia",
    estado: "GO"
  },
  {
    nome: "Goiânia Shopping",
    endereco: "Av T10, 1300, Setor Bueno - Quadra A, Lote 10/18 - 1º Piso Loja 163/164",
    contato: "https://api.whatsapp.com/send?phone=556239245165&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Goiânia",
    estado: "GO"
  },
  {
    nome: "Buriti Shopping Rio Verde",
    endereco: "R O, 1044 Quadra 015 Lote 01 - 1º Piso - Loja 007/008",
    contato: "https://api.whatsapp.com/send?phone=5562996031718&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Rio Verde",
    estado: "GO"
  },
  {
    nome: "Shopping Sul",
    endereco: "Br 040 Km 12, Gleba F, S/N , Qd 01, Lt 01 - Piso Térreo",
    contato: "https://api.whatsapp.com/send?phone=5562996818193&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Val Paraiso",
    estado: "GO"
  },
  {
    nome: "Tocantins Shopping",
    endereco: "R Piaui, 580 - 1º Piso - Quiosque 12",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Imperatriz",
    estado: "MA"
  },
  {
    nome: "Imperial Shopping",
    endereco: "RodiviaBR-010, 100 - Piso Imperial",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Imperatriz",
    estado: "MA"
  },
  {
    nome: "São Luis Shopping",
    endereco: "Av. Professor Carlos Cunha, 1000 - Loja 204",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Luis",
    estado: "MA"
  },
  {
    nome: "Rio Anil Shopping",
    endereco: "Av São Luis Rei De Franca, 08 - Loja 1048",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Luis",
    estado: "MA"
  },
  {
    nome: "Shopping da Ilha",
    endereco: "Av Daniel De La Touche, 987 - Loja 213",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Luis",
    estado: "MA"
  },
  {
    nome: "Pátio Norte Shopping",
    endereco: "Estrada de São Jose de Ribamar MA-201 - Lojas 109",
    contato: "https://api.whatsapp.com/send?phone=5584999860560&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São José de Ribamar",
    estado: "MA"
  },
  {
    nome: "DiamondMall",
    endereco: "Av. Olegário Maciel, 1600 - 1º Piso - Loja BG44/45",
    contato: "https://api.whatsapp.com/send?phone=5531981150691&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belo Horizonte",
    estado: "MG"
  },
  {
    nome: "Boulevard Shopping",
    endereco: "Av. Dos Andradas, 3000 - Loja 1050",
    contato: "https://api.whatsapp.com/send?phone=5531981150691&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belo Horizonte",
    estado: "MG"
  },
  {
    nome: "Minas Shopping",
    endereco: "Av. Olegário Maciel, 1600 - 1º Piso - Loja BG44/45",
    contato: "https://api.whatsapp.com/send?phone=5531992080755&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belo Horizonte",
    estado: "MG"
  },
  {
    nome: "BH Shopping",
    endereco: "Av. Olegário Maciel, 1600 - 1º Piso - Loja BG44/45",
    contato: "https://api.whatsapp.com/send?phone=5531991140664&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belo Horizonte",
    estado: "MG"
  },
  {
    nome: "Shopping Estação BH",
    endereco: "Av Cristiano Machado, 11833 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5531981162306&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belo Horizonte",
    estado: "MG"
  },
  {
    nome: "Shopping Del Rey",
    endereco: "Av. Presidente Carlos Luz, 3001 - Loja 1049",
    contato: "https://api.whatsapp.com/send?phone=5531981162306&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belo Horizonte",
    estado: "MG"
  },
  {
    nome: "Shopping Cidade",
    endereco: "Rua dos Tupis, 337 - 3º Piso - Loja G04",
    contato: "https://api.whatsapp.com/send?phone=5531981162306&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belo Horizonte",
    estado: "MG"
  },
  {
    nome: "ItauPower Shopping",
    endereco: "Av. General David Sarnoff, 5160 - 1º Piso - Loja 161",
    contato: "https://api.whatsapp.com/send?phone=5531981150691&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Contagem",
    estado: "MG"
  },
  {
    nome: "Shopping Contagem",
    endereco: "Avenida Severino Ballesteros, 99 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5531991140664&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Contagem",
    estado: "MG"
  },
  {
    nome: "Shopping do Vale do Aço",
    endereco: "Av Pedro Linhares Gomes, 3900 -1º Piso - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5531981162726&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Ipatinga",
    estado: "MG"
  },
  {
    nome: "Independência Shopping",
    endereco: "Av Presidente Itamar Franco, 3600 - Loja 155/156",
    contato: "https://api.whatsapp.com/send?phone=5531991140664&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Juiz de Fora",
    estado: "MG"
  },
  {
    nome: "Shopping Jardim Norte",
    endereco: "Av. Brasil, 6345 - Quiosque 105",
    contato: "https://api.whatsapp.com/send?phone=5531991140664&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Juiz de Fora",
    estado: "MG"
  },
  {
    nome: "Montes Claros Shopping",
    endereco: "Av. Donato Quintino, 90 - 1º Piso - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5531992080755&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Montes Claros",
    estado: "MG"
  },
  {
    nome: "Shopping Sete Lagoas",
    endereco: "Av. Otávio Campelo Ribeiro, 2801 - Loja SSI00280",
    contato: "https://api.whatsapp.com/send?phone=5531992080755&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Sete Lagoas",
    estado: "MG"
  },
  {
    nome: "Uberlandia Shopping",
    endereco: "Av. Paulo Gracindo, 15 - 1° Piso/ LJ 34",
    contato: "https://api.whatsapp.com/send?phone=5531981150691&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Uberlandia",
    estado: "MG"
  },
  {
    nome: "Center Shopping Uberlândia",
    endereco: "Av. Joao Naves De Avila, 1331 - 1° Piso / Loja 1214",
    contato: "https://api.whatsapp.com/send?phone=553432358236&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Uberlandia",
    estado: "MG"
  },
  {
    nome: "Shopping Uberaba",
    endereco: "Av. Santa Beatriz da Silva, 1501 - Loja 256",
    contato: "https://api.whatsapp.com/send?phone=553432358236&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Uberaba",
    estado: "MG"
  },
  {
    nome: "Shopping Campo Grande",
    endereco: "Av Afonso Pena, 4909 - Loja 2019",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Consulte o horário de atendimento",
    cidade: "Campo Grande",
    estado: "MS"
  },
  {
    nome: "Norte Sul Plaza",
    endereco: "Av. Presidente Ernesto Geisel, 2300 - Piso Térreo - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Consulte o horário de atendimento",
    cidade: "Campo Grande",
    estado: "MS"
  },
  {
    nome: "Shopping 3 Américas",
    endereco: "Av Brasília, 146 - 1º Piso - Bloco B - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Goiabeiras Shopping",
    endereco: "Av General Neves, 121 - 2º Piso - Loja 177",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Pantanal Shopping",
    endereco: "Av. Historiador Rubens De Mendonca, 3300 - Piso 2 - Quiosque 230",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Pantanal Shopping",
    endereco: "Av. Historiador Rubens De Mendonca, 3300 - 2º Piso - Loja 2026/2027",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Shopping Estação Cuiabá",
    endereco: "Av. Miguel sutil, 9300 loja 201 - 2 piso - 2º Piso - Quiosque 201",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Pantanal Shopping Kiosk 2",
    endereco: "Av. Historiador Rubens de Mendonça, 3300 - 2º Piso - Quiosque 103A",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Shopping Estação Cuiabá",
    endereco: "Av. Miguel sutil, 9300 loja 201 - 2 piso - Piso L1 - Loja 1125",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Shopping 3 Américas Kiosk 2",
    endereco: "Av Brasília, 146 - 2º Piso - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Cuiabá",
    estado: "MT"
  },
  {
    nome: "Rondon Plaza Shopping",
    endereco: "Av Rotary Internacional, 1950 - 1º Piso - Loja 208/209",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Rondonópolis",
    estado: "MT"
  },
  {
    nome: "Park Shopping Sorriso",
    endereco: "Av Tancredo Neves, 543 - Quiosque 04",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Sorriso",
    estado: "MT"
  },
  {
    nome: "Várzea Grande Shopping",
    endereco: "Rua Presidente Arthur Bernardes S/N - 1º Piso - Loja 208",
    contato: "https://api.whatsapp.com/send?phone=5567991607785&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Várzea Grande",
    estado: "MT"
  },
  {
    nome: "Shopping Metropole Ananindeua",
    endereco: "Rodovia BR 316, KM 04 Nº 4500 - Quiosque 207A",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Ananindeua",
    estado: "PA"
  },
  {
    nome: "Boulevard Shopping Belém",
    endereco: "Av. Visconde de Sousa Franco, 776 - Loja 110",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belém",
    estado: "PA"
  },
  {
    nome: "Shopping Pátio Belém",
    endereco: "Tv Padre Eutiquio, 1078 - 1º Piso - Loja 109",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belém",
    estado: "PA"
  },
  {
    nome: "Parque Shopping Belem",
    endereco: "Rod. Augusto Montenegro, 4300 - 1º Piso - Loja 1027/1028",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belém",
    estado: "PA"
  },
  {
    nome: "Boulevard Shopping Belem",
    endereco: "Av. Visconde De Souza Franco, 776 - Quiosque 113",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Belém",
    estado: "PA"
  },
  {
    nome: "Shopping Pátio Marabá",
    endereco: "Q Quadra 15 Folha 30 Loe 10 S/N - Loja 248/249",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Marabá",
    estado: "PA"
  },
  {
    nome: "Partage Shopping Parauapebas",
    endereco: "Rod. PA 275 KM 61,6 Lote 01A - Loja 44/45",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Parauapebas",
    estado: "PA"
  },
  {
    nome: "Partage Shopping - Campina Grande",
    endereco: "Av. Pref. Severino Bezerra Cabral, 1050 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Campina grande",
    estado: "PB"
  },
  {
    nome: "Manaíra Shopping",
    endereco: "Av Governador Flavio Ribeiro Coutinho, 220 - Loja T0401",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "João Pessoa",
    estado: "PB"
  },
  {
    nome: "Mangabeira Shopping",
    endereco: "Av Governador Flavio Ribeiro Coutinho, 220 - Loja T0401",
    contato: "https://api.whatsapp.com/send?phone=5583991823631&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "João Pessoa",
    estado: "PB"
  },
  {
    nome: "Caruaru Shopping",
    endereco: "Av. Adjar Da Silva Case, 800 - Loja 20",
    contato: "https://api.whatsapp.com/send?phone=5583991823631&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Caruaru",
    estado: "PE"
  },
  {
    nome: "Shopping Guararapes",
    endereco: "Av Barreto de Menezes, 800 - Loja 237",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Jaboatão dos Guararapes",
    estado: "PE"
  },
  {
    nome: "Shopping Patteo Olinda",
    endereco: "Rua Carmelita Soares Muniz de Araújo, 225 setor 07 - 1º Piso - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Olinda",
    estado: "PE"
  },
  {
    nome: "Shopping RioMar Recife",
    endereco: "Av. República Do Líbano, 251 - Loja 1141",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Recife",
    estado: "PE"
  },
  {
    nome: "Shopping Tacaruna",
    endereco: "Av Governador Agamenon Magalhães, 153 - 1º Piso - Loja 47",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Recife",
    estado: "PE"
  },
  {
    nome: "Shopping Plaza - Casa Forte",
    endereco: "Rua Doutor João Santos Filho, 255 - Piso L3 - Loja 156/157",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Recife",
    estado: "PE"
  },
  {
    nome: "Shopping Recife",
    endereco: "R. Padre Carapuceiro, 777 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Recife",
    estado: "PE"
  },
  {
    nome: "Shopping Boa Vista",
    endereco: "Rua do Giriquiti, 48 - 1º Piso - Loja 121/122",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Recife",
    estado: "PE"
  },
  {
    nome: "Teresina Shopping",
    endereco: "Av Raul Lopes, 1000 -1º Piso - Loja 238",
    contato: "https://api.whatsapp.com/send?phone=558632150405&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Teresina",
    estado: "PI"
  },
  {
    nome: "Shopping Rio Poty",
    endereco: "Av. Marechal Castelo Branco, 911 - Piso L3 - Loja 307H",
    contato: "https://api.whatsapp.com/send?phone=558632150405&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Teresina",
    estado: "PI"
  },
  {
    nome: "Park Shopping Barigui",
    endereco: "Rua: Pedro Viriato Parigot De Sousa, 600 - Loja 275",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Park Shopping Barigui",
    endereco: "Rua Pedro Viriato Parigot De Sousa, 600 - Quiosque 145",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Shopping Palladium Curitiba",
    endereco: "Av. Presidente Kennedy, 4121 - Quiosque 118",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Shopping Mueller Curitiba",
    endereco: "Av Candido De Abreu, 127 - Loja 50/51",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Shopping Estação",
    endereco: "Av. Sete de Setembro, 2775 - Piso L1 - Loja 1108 A",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Shopping Curitiba",
    endereco: "Rua Brigadeiro Franco, 2300 - Piso L3",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Shopping Palladium",
    endereco: "Av. Presidente Kennedy, 4121 - Piso L1 - Loja 1057",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Jockey Plaza Shopping Center",
    endereco: "Av. Victor Ferreira do Amaral, 2633 - Piso L1 - Quiosque 167",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Curitiba",
    estado: "PR"
  },
  {
    nome: "Catuai Shopping Londrina",
    endereco: "Rodovia Celso Garcia Cid, Km.377 - Loja 82/83",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Londrina",
    estado: "PR"
  },
  {
    nome: "Londrina Norte",
    endereco: "Av. Americo Deolindo Garla, 224 -Piso Térreo - Quiosque",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Londrina",
    estado: "PR"
  },
  {
    nome: "Boulevard Londrina Shopping",
    endereco: "Av. Theodoro Victorelli, 150 - 1º Piso - Loja 76",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Londrina",
    estado: "PR"
  },
  {
    nome: "Catuai Shopping Maringa",
    endereco: "Av Colombo, 9161 - Loja 157/158",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Maringá",
    estado: "PR"
  },
  {
    nome: "Maringa Park Shopping Center",
    endereco: "Av. São Paulo, 1099 - 1º Piso - Quiosque 130",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Maringá",
    estado: "PR"
  },
  {
    nome: "Palladium Shopping Center Ponta Grossa",
    endereco: "Rua Ermelino de Leão, 703 - Piso Térreo - Loja 61/62",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Ponta Grossa",
    estado: "PR"
  },
  {
    nome: "Shopping Sao José",
    endereco: "R Izabel A Redentora, 1434 - Quiosque 103",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "São José dos Pinhais",
    estado: "PR"
  },
  {
    nome: "Guarus Plaza Shopping",
    endereco: "Avenida Senador José Carlos Pereira Pinto, S/N - 1º Piso - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5521988140553&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Campo dos Goytacazes",
    estado: "RJ"
  },
  {
    nome: "Boulevard Shopping Campos",
    endereco: "Av Doutor Silvio Bastos Tavares, 330 - Loja 104 ",
    contato: "https://api.whatsapp.com/send?phone=5521988140553&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Campo dos Goytacazes",
    estado: "RJ"
  },
  {
    nome: "Shopping Park Lagos",
    endereco: "Av. Heriquente Terra, 1700 - 1º Piso - Quiosque 25",
    contato: "https://api.whatsapp.com/send?phone=5522988279441&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Cabo Frio",
    estado: "RJ"
  },
  {
    nome: "Caxias Shopping",
    endereco: "Rod. Washington Luiz, 2.895 - Loja 210D",
    contato: "https://api.whatsapp.com/send?phone=5521971808908&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Duque de Caxias",
    estado: "RJ"
  },
  {
    nome: "Itaguai Shopping Center",
    endereco: "Rua Doutor Curvelo Cavalcanti, 189 - 1º Piso - Quiosque 3",
    contato: "https://api.whatsapp.com/send?phone=5521970398075&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Itaguai",
    estado: "RJ"
  },
  {
    nome: "Shopping Plaza Macae",
    endereco: "Av Aloisio da Silva Gomes, 800 - Loja 104",
    contato: "https://api.whatsapp.com/send?phone=5521971662383&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Macaé",
    estado: "RJ"
  },
  {
    nome: "Nilópolis Square Shopping Center",
    endereco: "Rua Professor Alfredo Gonçalves Filgueiras, 100 - 1º Piso - Quiosque 07",
    contato: "https://api.whatsapp.com/send?phone=5521970398075&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Nilópolis",
    estado: "RJ"
  },
  {
    nome: "Niterói Shopping",
    endereco: "R Xv De Novembro, 08 - Loja 167",
    contato: "https://api.whatsapp.com/send?phone=5521999328623&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Niterói",
    estado: "RJ"
  },
  {
    nome: "TopShopping Nova Iguaçu",
    endereco: "Av. Governador Roberto Silveira, 540 - 1º Piso - Loja 134/135",
    contato: "https://api.whatsapp.com/send?phone=5521971662383&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Nova Iguaçu",
    estado: "RJ"
  },
  {
    nome: "Cadima Shopping",
    endereco: "Rua Moisés Amélio, 17 - Quiosque 1",
    contato: "https://api.whatsapp.com/send?phone=5524981118850&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Nova Friburgo",
    estado: "RJ"
  },
  {
    nome: "West Shopping",
    endereco: "Est Do Mendanha, 555 - Loja 182/183",
    contato: "https://api.whatsapp.com/send?phone=5521988140553&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Shopping Metropolitano Barra",
    endereco: "Av Embaixador Abelardo Bueno, 1300 - Piso L1 - Loja 1057",
    contato: "https://api.whatsapp.com/send?phone=5521988140553&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Norte Shopping",
    endereco: "Av Dom Hélder Câmara, 5474 - Loja 2903",
    contato: "https://api.whatsapp.com/send?phone=5521971808908&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Shopping Tijuca",
    endereco: "Av Maracanã, 987 - Loja 17/18",
    contato: "https://api.whatsapp.com/send?phone=5521971808908&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Park Shopping Campo Grande",
    endereco: "Est Do Monteiro, 1200 - Piso L1 - Loja 107",
    contato: "https://api.whatsapp.com/send?phone=5521971662383&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Via Parque Shopping",
    endereco: "Av. Ayrton Senna, 3000 - 1º Piso - Loja 1092",
    contato: "https://api.whatsapp.com/send?phone=5521971662383&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Via Brasil Shopping",
    endereco: "R Itapera, 500 - Loja 382/383",
    contato: "https://api.whatsapp.com/send?phone=5521973623246&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Nova América Shopping",
    endereco: "Av Pastor Martin Luther King Junior, 126 - Loja 1427",
    contato: "https://api.whatsapp.com/send?phone=5521973623246&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Shopping Leblon",
    endereco: "Av Afranio De Melo Franco, 290 - Loja 108",
    contato: "https://api.whatsapp.com/send?phone=5521971689653&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Barra Shopping",
    endereco: "Av. das Américas, 4666 - Loja 108/114",
    contato: "https://api.whatsapp.com/send?phone=5521971689653&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Shopping Rio Sul",
    endereco: "Rua Lauro Sodré, 445 - 3º Piso - Loja 301",
    contato: "https://api.whatsapp.com/send?phone=5521971689653&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Santa Cruz Shopping",
    endereco: "Rua Felipe Cardoso, 540 - Quiosque 07",
    contato: "https://api.whatsapp.com/send?phone=5521970398075&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Center Shopping Rio",
    endereco: "Rua Geremario Dantas, 404 - Piso L1 - Quiosque 11",
    contato: "https://api.whatsapp.com/send?phone=5521970398075&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Bangu Shopping",
    endereco: "R Fonseca, 240 - Quiosque 58",
    contato: "https://api.whatsapp.com/send?phone=5521988618998&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Bangu Shopping",
    endereco: "R Fonseca, 240 - Loja N124/124A",
    contato: "https://api.whatsapp.com/send?phone=5521988618998&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Passeio Shopping",
    endereco: "Rua Viuva Dantas, 100 - Loja 101/103",
    contato: "https://api.whatsapp.com/send?phone=5521988618998&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Shopping Boulevard",
    endereco: "Rua Barão de São Francisco, 236 - Quiosque 103",
    contato: "https://api.whatsapp.com/send?phone=5521988618998&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Madureira Shopping",
    endereco: "Estrada do Portela, 222 - Quiosque L1Q06E",
    contato: "https://api.whatsapp.com/send?phone=5521964749199&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Parque Shopping Sulacap",
    endereco: "Av. Marechal Fontenelle, 3545 - Quiosque MKT 22",
    contato: "https://api.whatsapp.com/send?phone=5521964749199&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Ilha Plaza Shopping",
    endereco: "AV. Maestro Paulo e Silva, 400 - 2º Piso Quiosque 213B",
    contato: "https://api.whatsapp.com/send?phone=5521964749199&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Shopping Jardim Guadalupe",
    endereco: "Av. Brasil, 22155 - 1º Piso - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5521970398137&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Carioca Shopping",
    endereco: "Av. Vicente de Carvalho, 909 - 1º Piso - Loja 103",
    contato: "https://api.whatsapp.com/send?phone=5521970398137&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Américas Shopping",
    endereco: "Av. das Américas, 15500 - Loja L1",
    contato: "https://api.whatsapp.com/send?phone=5521970398137&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Recreio Shopping",
    endereco: "Av. das Américas 19019 - Quiosque 13",
    contato: "https://api.whatsapp.com/send?phone=5521970398137&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio de Janeiro",
    estado: "RJ"
  },
  {
    nome: "Shopping Grande Rio",
    endereco: "Rua Marias Soares Sendas, 111 - Loja 263/65",
    contato: "https://api.whatsapp.com/send?phone=5521971662383&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São João do Meriti",
    estado: "RJ"
  },
  {
    nome: "São Gonçalo Shopping",
    endereco: "Av. São Goncalo, 100 - Piso L2 - Loja 213/214",
    contato: "https://api.whatsapp.com/send?phone=5521999328623&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Gonçalo",
    estado: "RJ"
  },
  {
    nome: "Partage Shopping São Gonçalo",
    endereco: "Av. Presidente Kennedy,425 - Piso L1 - Loja 155",
    contato: "https://api.whatsapp.com/send?phone=5521999328623&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Gonçalo",
    estado: "RJ"
  },
  {
    nome: "Pátio Alcântara",
    endereco: "Praça Carlos Gianelli, S/N - L2 - Quiosque 25",
    contato: "https://api.whatsapp.com/send?phone=5522988279441&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Gonçalo",
    estado: "RJ"
  },
  {
    nome: "Sider Shopping",
    endereco: "Rua Doze, 300 - Quiosque M209",
    contato: "https://api.whatsapp.com/send?phone=5524981118850&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Volta Redonda",
    estado: "RJ"
  },
  {
    nome: "Teresópolis Shopping",
    endereco: "R. Edmundo Bitencourt, 101 - 1º Piso - Quosque 207",
    contato: "https://api.whatsapp.com/send?phone=5521988618998&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Teresópolis",
    estado: "RJ"
  },
  {
    nome: "Natal Shopping",
    endereco: "Av Senador Salgado Filho, 2234 - 2º Piso - Quiosque 08",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Natal",
    estado: "RN"
  },
  {
    nome: "Partage Norte Shopping Natal",
    endereco: "Av Doutor João Medeiros Filho, 2395 - 1º Piso - Quiosque 06",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Natal",
    estado: "RN"
  },
  {
    nome: "Midway Mall",
    endereco: "Av Bernardo Vieira, 3775 - Piso L3 - Loja 372/372A",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Natal",
    estado: "RN"
  },
  {
    nome: "Porto Velho Shopping",
    endereco: "Av Rio Madeira, 3288 - Loja 230/231",
    contato: "https://api.whatsapp.com/send?phone=559284174781&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Porto Velho",
    estado: "RO"
  },
  {
    nome: "Shopping Bento",
    endereco: "Rua Marechal Deodoro, 238 - Quiosque 74",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Bento Gonçalves",
    estado: "RS"
  },
  {
    nome: "Bourbon Shopping Novo Hamburgo",
    endereco: "Av Nações Unidas, 2001 - Loja 1062",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Novo Hamburgo",
    estado: "RS"
  },
  {
    nome: "Bella Città Shopping Center",
    endereco: "R Coronel Chicuta, 355 - Loja 11",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Passo Fundo",
    estado: "RS"
  },
  {
    nome: "Shopping Pelotas",
    endereco: "Av. Ferreira Viana, 1526 - Loja 28",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Pelotas",
    estado: "RS"
  },
  {
    nome: "Shopping Total",
    endereco: "Av Cristovão Colombo, 545 - Quiosque 38",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Shopping Iguatemi Porto Alegre",
    endereco: "Av Joao Wallig, 1800 - Loja 124",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Bourbon Shopping Ipiranga",
    endereco: "Av Ipiranga, 5200 - Loja 131",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Bourbon Country",
    endereco: "Av Tulio De Rose, 80 - Loja 131",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Barra Shopping Sul",
    endereco: "Av. Diário De Notícias, 300 - Loja 1042",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Praia de Belas Shopping",
    endereco: "Av Praia De Belas, 1181 - Loja 3060",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Bourbon Shopping Wallig",
    endereco: "Av Assis Brasil, 2611 - Loja 203",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Porto Alegre Center Lar",
    endereco: "Av. Sertorio, 8000 - Loja 132/133/134",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Porto Alegre",
    estado: "RS"
  },
  {
    nome: "Royal Plaza Shopping",
    endereco: "Av Nossa Senhora Das Dores, 305 - Loja 360",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Santa Maria",
    estado: "RS"
  },
  {
    nome: "Bourbon Shopping São Leopoldo",
    endereco: "R Primeiro De Março, 821 - Loja 289/290",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "São Leoplodo",
    estado: "RS"
  },
  {
    nome: "Shopping Iguatemi Caxias do Sul",
    endereco: "Av. Rio Branco, 452 - Loja QN 03",
    contato: "https://api.whatsapp.com/send?phone=5554992397958&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Caxias do Sul",
    estado: "RS"
  },
  {
    nome: "Canoas Shopping",
    endereco: "Av Guilherme Schell, 6750 - Loja A74/75",
    contato: "https://api.whatsapp.com/send?phone=5551989566770&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Canoas",
    estado: "RS"
  },
  {
    nome: "Parque Shopping Canoas",
    endereco: "Av. Farroupilha, 4545 - Loja 2066",
    contato: "https://api.whatsapp.com/send?phone=5551989566770&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Consulte o horário de atendimento",
    cidade: "Canoas",
    estado: "RS"
  },
  {
    nome: "Neumarkt Shopping",
    endereco: "R Sete De Setembro, 1213 - Loja 126",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Blumenau",
    estado: "SC"
  },
  {
    nome: "Norte Shopping Blumenau - 3000",
    endereco: "Rod Br 470, 3000",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Blumenau",
    estado: "SC"
  },
  {
    nome: "Balneário Shopping",
    endereco: "Av. Santa Catarina, 01 - Loja 136",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Camboriu",
    estado: "SC"
  },
  {
    nome: "Shopping Pátio Chapecó",
    endereco: "Av Fernando Machado, 4000 - Loja 06",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Chapecó",
    estado: "SC"
  },
  {
    nome: "Nações Shopping",
    endereco: "Avenida Jorge Elias De Lucca, 677 - Loja 124",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Criciuma",
    estado: "SC"
  },
  {
    nome: "Shopping Iguatemi Florianópolis",
    endereco: "Av. Madre Benvenuta, 687 - Loja 269",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Florianópolis",
    estado: "SC"
  },
  {
    nome: "Floripa Shopping",
    endereco: "Rod. José Carlos Daux, 587 - Quiosque",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Florianópolis",
    estado: "SC"
  },
  {
    nome: "Itajaí Shopping",
    endereco: "Rua Samuel Heusi, 234 - Quiosque 07",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Itajai",
    estado: "SC"
  },
  {
    nome: "Jaraguá do Sul Park Shopping",
    endereco: "Avenida Getúlio Vargas, 268 - 2º Piso - Quiosque",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Jaraguá do Sul",
    estado: "SC"
  },
  {
    nome: "Garten Shopping",
    endereco: "R. Rolf Wiest, 333 - Loja 114",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Joinville",
    estado: "SC"
  },
  {
    nome: "Shopping Mueller Joinville",
    endereco: "Rua Senador Felipe Schmidt, 235 - Loja 85",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Joinville",
    estado: "SC"
  },
  {
    nome: "Lages Garden Shopping",
    endereco: "Rod Br 282 Km 216 S/N - Loja 24/25",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Lages",
    estado: "SC"
  },
  {
    nome: "Shopping ViaCatarina",
    endereco: "Av Atílio Pedro Pagani, 270 - 1º Piso - Quiosque 3",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Palhoça",
    estado: "SC"
  },
  {
    nome: "Continente Park Shopping",
    endereco: "Rod Br 101 - Sn - Km 211 - Loja 104",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "São José",
    estado: "SC"
  },
  {
    nome: "Shopping Itaguaçu",
    endereco: "R Geroncio Thives, 1079 - Loja 257",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "São José",
    estado: "SC"
  },
  {
    nome: "Farol Shopping",
    endereco: "Av Marcolino Martins Cabral, 2525 - Loja 10",
    contato: "",
    horario: "Consulte o horário de atendimento",
    cidade: "Tubarao",
    estado: "SC"
  },
  {
    nome: "Shopping Jardins",
    endereco: "Av Ministro Geraldo Barreto Sobral, 215 - 1º Piso - Loja 127",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Aracaju",
    estado: "SE"
  },
  {
    nome: "Shopping RioMar Aracaju",
    endereco: "Av. Delmiro Gouveia ,400 - Térreo - Loja 137",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Aracaju",
    estado: "SE"
  },
  {
    nome: "Shopping Premio",
    endereco: "Av Coletora A, S/N - Piso Térreo - Loja 71",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Nossa Senhora do Socorro",
    estado: "SE"
  },
  {
    nome: "Shopping Praça Nova Araçatuba",
    endereco: "Rua Carlos Pereira da Silva, 6.000 - 1° Piso - Quiosque 113",
    contato: "https://api.whatsapp.com/send?phone=5519997189329&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Araçatuba",
    estado: "SP"
  },
  {
    nome: "Shopping Jaragua",
    endereco: "Av. Alberto Benassi, 2270 - Loja 67/68",
    contato: "https://api.whatsapp.com/send?phone=5519997189329&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Araraquara",
    estado: "SP"
  },
  {
    nome: "Shopping Tamboré",
    endereco: "Av Piracema , 669 - Piso Térreo - Loja 481",
    contato: "https://api.whatsapp.com/send?phone=5511975061046&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Barueri",
    estado: "SP"
  },
  {
    nome: "Shopping Iguatemi Alphaville",
    endereco: "Alameda Rio Negro, 111 - Xingu - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511958619627&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Barueri",
    estado: "SP"
  },
  {
    nome: "Boulevard Shopping Bauru",
    endereco: "Rua General Marcondes Salgado, 11 - Piso L1 / Loja 162/163",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Bauru",
    estado: "SP"
  },
  {
    nome: "Bauru Shopping",
    endereco: "Rua Henrique Savi, 55 - Quadra 15 - 1ª andar",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Bauru",
    estado: "SP"
  },
  {
    nome: "Shopping Granja Vianna",
    endereco: "Rod Raposo Tavares, 400 - Loja 367",
    contato: "https://api.whatsapp.com/send?phone=5511975061046&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Cotia",
    estado: "SP"
  },
  {
    nome: "Parque Dom Pedro Shopping",
    endereco: "Av. Guilherme Campos, 500 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5519997189329&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Campinas",
    estado: "SP"
  },
  {
    nome: "Shopping Parque das Bandeiras",
    endereco: "Av John Boyd Dunlop , 3900 - Piso L3 - Quiosque 302",
    contato: "https://api.whatsapp.com/send?phone=5519989882027&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Campinas",
    estado: "SP"
  },
  {
    nome: "Campinas Shopping",
    endereco: "Av. Guilherme Campos, 500 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5519989882027&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Campinas",
    estado: "SP"
  },
  {
    nome: "Iguatemi Campinas",
    endereco: "Av. Guilherme Campos, 500 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5519989882027&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Campinas",
    estado: "SP"
  },
  {
    nome: "Shopping Praça da Moça",
    endereco: "Rua Manoel Da Nobrega, 712 - Piso Paineiras - Quiosque 13",
    contato: "https://api.whatsapp.com/send?phone=5511932354339&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Diadema",
    estado: "SP"
  },
  {
    nome: "Franca Shopping",
    endereco: "Av. Rio Negro, 1100 - Loja 410/411/412",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Franca",
    estado: "SP"
  },
  {
    nome: "Buriti Shopping Guara",
    endereco: "Av Juscelino Kubtscheck de oliveira, 300 - 1º Piso - Quiosque Samsung",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Guaratingueta",
    estado: "SP"
  },
  {
    nome: "Parque Shopping Maia",
    endereco: "Avenida Bartolomeu de Carlos, 230",
    contato: "https://api.whatsapp.com/send?phone=5511975324763&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Guarulhos",
    estado: "SP"
  },
  {
    nome: "Internacional Shopping",
    endereco: "Rua Engenheiro Camilo Olivetti, 295 - Loja 04/05",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Guarulhos",
    estado: "SP"
  },
  {
    nome: "Internacional Shopping",
    endereco: "Rua Engenheiro Camilo Olivetti, 295 - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Guarulhos",
    estado: "SP"
  },
  {
    nome: "Plaza Shopping Itu",
    endereco: "Av. Dr. Ermelindo Maffei, 1199 - 1º Piso - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511958619627&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Itu",
    estado: "SP"
  },
  {
    nome: "Itaqua Garden Shopping",
    endereco: "Estrada Municipal do Mandi, 1205 - Piso Superior - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511975324763&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Itaquaquecetuba",
    estado: "SP"
  },
  {
    nome: "Polo Shopping Indaiatuba",
    endereco: "Alameda Filtros Mann, 600 - Térreo - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5519989882027&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Indaiatuba",
    estado: "SP"
  },
  {
    nome: "Jundiaí Shopping",
    endereco: "Av Nove De Julho, 3333 - 1º Piso - Loja 132",
    contato: "https://api.whatsapp.com/send?phone=5519997189329&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Jundiai",
    estado: "SP"
  },
  {
    nome: "Maxi Shopping Jundiai",
    endereco: "Av. Antonio Frederico Ozanam, 6000 - 1º Piso - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5519997189329&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Jundiai",
    estado: "SP"
  },
  {
    nome: "Patio Limeira Shopping",
    endereco: "Rua Carlos Gomes, 1321 - 1º Piso - Quiosque 64",
    contato: "https://api.whatsapp.com/send?phone=5519989882027&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "Limeira",
    estado: "SP"
  },
  {
    nome: "Maua Plaza Shopping",
    endereco: "Av Governador Mario Covas Junior, 1 - Loja 25/27",
    contato: "https://api.whatsapp.com/send?phone=551332018181&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Maua",
    estado: "SP"
  },
  {
    nome: "Marília Shopping",
    endereco: "Rua dos Tucunarés, 500 - Quiosque 13",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Marilia",
    estado: "SP"
  },
  {
    nome: "Mogi Shopping",
    endereco: "Av. Vereador Narciso Yague Gumaraes, 1001 - Loja 415/416",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Mogi das Cruzes",
    estado: "SP"
  },
  {
    nome: "Buriti Shopping Mogi Guaçu",
    endereco: "Rua Francisco Franco de Godoi Bueno, 801 - Quiosque 27",
    contato: "https://api.whatsapp.com/send?phone=5519997189329&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Mogi Guaçú",
    estado: "SP"
  },
  {
    nome: "Osasco Plaza Shopping",
    endereco: "R Tenente Avelar Pires De Azevedo, 81 - Loja 158/159",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Osasco",
    estado: "SP"
  },
  {
    nome: "Shopping União de Osasco",
    endereco: "Av Dos Autonomistas, 1400 - Arco 305",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Osasco",
    estado: "SP"
  },
  {
    nome: "Super Shopping Osasco",
    endereco: "Av. Dos Autonomistas, 1828 - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Osasco",
    estado: "SP"
  },
  {
    nome: "Litoral Plaza Shopping",
    endereco: "Av Ayrton Senna da Silva, 1511 - Loja 62",
    contato: "https://api.whatsapp.com/send?phone=551332018181&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Praia Grande",
    estado: "SP"
  },
  {
    nome: "Shopping Piracicaba",
    endereco: "Av. Limeira, 722 - 1º Piso - Loja 108A/109",
    contato: "https://api.whatsapp.com/send?phone=5519997189329&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Piracicaba",
    estado: "SP"
  },
  {
    nome: "Prudenshopping",
    endereco: "Av Manoel Goulart, 2400 - Loja 1030/1031",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Presidente Prudente",
    estado: "SP"
  },
  {
    nome: "Ribeirão Shopping",
    endereco: "Av. Cel. Fernando Ferreira Leite, 1540 - 1° Piso Loja 147",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Ribeirão Preto",
    estado: "SP"
  },
  {
    nome: "Novo Shopping Center",
    endereco: "Av. Presidente Kennedy, 1500 - Loja 235 - Loja 235",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Ribeirão Preto",
    estado: "SP"
  },
  {
    nome: "Shopping Santa Úrsula",
    endereco: "Rua São José, 933 - Piso Bandeirantes - 1 - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Ribeirão Preto",
    estado: "SP"
  },
  {
    nome: "Shopping Rio Claro",
    endereco: "Av. Conde Francisco Matarazzo Junior, 205 - Quiosque 60",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Rio Claro",
    estado: "SP"
  },
  {
    nome: "Patio Ciane Shopping",
    endereco: "Av Afonso Vergueiro, 823 - 2º Piso - Quiosque 29",
    contato: "https://api.whatsapp.com/send?phone=5519997189329&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Sorocaba",
    estado: "SP"
  },
  {
    nome: "Iguatemi São Carlos",
    endereco: "Passeio Dos Flamboyants, 200 - Quiosque 109",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Carlos",
    estado: "SP"
  },
  {
    nome: "Plaza Avenida Shopping",
    endereco: "Av Jose Munia, 4775 - 2° Piso / Loja 230",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São José do Rio Preto",
    estado: "SP"
  },
  {
    nome: "RioPreto Shopping Center",
    endereco: "Av. Brigadeiro Faria Lima, 6363 - Loja 257",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São José do Rio Preto",
    estado: "SP"
  },
  {
    nome: "Shopping Iguatemi São José do Rio Preto",
    endereco: "Av Presidente Jucelino Kubitschek de Oliveira, 5000 - Piso Térreo",
    contato: "https://api.whatsapp.com/send?phone=5517996535775&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São José do Rio Preto",
    estado: "SP"
  },
  {
    nome: "Tivoli Shopping",
    endereco: "Av. Santa Barbara, 777 - Térreo - Loja 16",
    contato: "https://api.whatsapp.com/send?phone=5519989882027&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Santa Barbara do Oeste",
    estado: "SP"
  },
  {
    nome: "Grand Plaza Shopping",
    endereco: "Av. Industrial, 600 - Quiosque 329",
    contato: "https://api.whatsapp.com/send?phone=5511932354339&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Santo André",
    estado: "SP"
  },
  {
    nome: "Shopping ABC",
    endereco: "Av Pereira Barreto, 42 - 2º Piso",
    contato: "https://api.whatsapp.com/send?phone=5511989158450&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Santo André",
    estado: "SP"
  },
  {
    nome: "Atrium Shopping",
    endereco: "Rua Giovanni Batista Pirelli, 155 - Loja 232",
    contato: "https://api.whatsapp.com/send?phone=551332018181&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Santo André",
    estado: "SP"
  },
  {
    nome: "Shopping Brisamar",
    endereco: "R Frei Gaspar, 365 - 2º Piso - Loja 211",
    contato: "https://api.whatsapp.com/send?phone=551332018181&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Vicente",
    estado: "SP"
  },
  {
    nome: "Praiamar Shopping",
    endereco: "R. Alexandre Martins, 80 - Loja 01",
    contato: "https://api.whatsapp.com/send?phone=551332018181&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Santos",
    estado: "SP"
  },
  {
    nome: "Miramar Shopping",
    endereco: "Av. Marechal Floriano Peixoto, 44 - Loja 53",
    contato: "https://api.whatsapp.com/send?phone=551332018181&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Santos",
    estado: "SP"
  },
  {
    nome: "Centervale Shopping",
    endereco: "Av Deputado Benedito Matarazzo, 9403 - 2º Piso - Loja 164",
    contato: "https://api.whatsapp.com/send?phone=5512997980784&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São José dos Campos",
    estado: "SP"
  },
  {
    nome: "Vale Sul Shopping",
    endereco: "Av. Andrômeda, 227 - 1º Piso - Loja 206/207",
    contato: "https://api.whatsapp.com/send?phone=5512997140108&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São José dos Campos",
    estado: "SP"
  },
  {
    nome: "ParkShopping São Caetano",
    endereco: "Al Terracota, 545 - Loja 1063",
    contato: "https://api.whatsapp.com/send?phone=5511989158450&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Caetano",
    estado: "SP"
  },
  {
    nome: "Suzano Shopping",
    endereco: "Rua Ste de Setembro, 555 - Loja 55",
    contato: "https://api.whatsapp.com/send?phone=5512997140108&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Suzano",
    estado: "SP"
  },
  {
    nome: "São Bernardo Plaza Shopping",
    endereco: "Av. Rotary, 624 - Piso L2 - Loja 2776",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Bernardo",
    estado: "SP"
  },
  {
    nome: "Shopping Metropole",
    endereco: "Praça Samuel Sabatini, 200 - Loja 224/225",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Bernardo",
    estado: "SP"
  },
  {
    nome: "Central Plaza Shopping",
    endereco: "Av Dr Francisco de Mesquita, 1000 - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511932354339&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Pátio Higienopolis",
    endereco: "Av. Higienópolis, 674 - Piso Pacaembu",
    contato: "https://api.whatsapp.com/send?phone=5511964240712&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Light",
    endereco: "Rua Coronel Xavier De Toledo, 23 - Terreo - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511964240712&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Metrô Santa Cruz",
    endereco: "R Domingos De Moraes, 2564 - Loja NL7/T",
    contato: "https://api.whatsapp.com/send?phone=5511964240712&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Cidade São Paulo",
    endereco: "Av Paulista, 1230 - Piso Térreo",
    contato: "https://api.whatsapp.com/send?phone=5511964240712&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Top Center",
    endereco: "Av Paulista, 854 - Piso Térreo - Quiosque 22",
    contato: "https://api.whatsapp.com/send?phone=5511964240712&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Villa Lobos",
    endereco: "Av. Das Nacoes Unidas, 4777 - Loja 359",
    contato: "https://api.whatsapp.com/send?phone=5511942441354&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Eldorado",
    endereco: "Av. Rebouças, 3970 - Loja 0248",
    contato: "https://api.whatsapp.com/send?phone=5511995977157&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping West Plaza",
    endereco: "Av Francisco Matarazzo, S/N - Anexo Arco 1104",
    contato: "https://api.whatsapp.com/send?phone=5511995977157&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Campo Limpo",
    endereco: "Est Do Campo Limpo, 459 - Loja 361",
    contato: "https://api.whatsapp.com/send?phone=5511995977157&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Boa Vista - Santo Amaro",
    endereco: "Rua Borba Gato, 59 - Piso L2 - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511995977157&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Raposo Shopping",
    endereco: "Rodovia Raposo Tavares, KM 14,5 - Piso Raposo - Loja 3079",
    contato: "https://api.whatsapp.com/send?phone=5511975061046&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung..",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Metrô Tucuruvi",
    endereco: "Av Doutor Antonio Maria Laet, 566 - Loja 11",
    contato: "https://api.whatsapp.com/send?phone=5511975324763&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Center Norte",
    endereco: "Tv Casalbuono, 120 - Loja 950",
    contato: "https://api.whatsapp.com/send?phone=5511975324763&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Metro Tatuape",
    endereco: "Avenida Radial Leste, S/N - Piso Tatuapé - Loja 129",
    contato: "https://api.whatsapp.com/send?phone=5511975324763&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Santana Parque Shopping",
    endereco: "R Conselheiro Moreira De Barros, 2780 - Piso Térreo - Loja 1043",
    contato: "https://api.whatsapp.com/send?phone=5511975324763&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Center Norte",
    endereco: "Tv Casalbuono, 120 - Quiosque 950",
    contato: "https://api.whatsapp.com/send?phone=5511975324763&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Metro Tatuape",
    endereco: "Av. Radial Leste, S/N - Piso Metrô - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511975324763&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Bourbon Shopping São Paulo",
    endereco: "Rua Turiassú, 2100 - Loja Suc T003",
    contato: "https://api.whatsapp.com/send?phone=5511948694670&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Vila Olímpia",
    endereco: "R Das Olimpiadas, 360 - Loja 110/111",
    contato: "https://api.whatsapp.com/send?phone=5511948694670&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Morumbi",
    endereco: "Av Roque Petroni Junior, 1089 - Loja N16",
    contato: "https://api.whatsapp.com/send?phone=5511948694670&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Parque da Cidade",
    endereco: "Av. das Nações Unidas,14401 - Piso Térreo",
    contato: "https://api.whatsapp.com/send?phone=5511948694670&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Morumbi Town",
    endereco: "Av. Giovanni Gronchi, 5930 - Piso Térreo - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511948694670&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Mooca Plaza Shopping",
    endereco: "Rua Capitao Pacheco Chaves, 313 - 2º Piso - Loja 2717",
    contato: "https://api.whatsapp.com/send?phone=5511988461131&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Anália Franco",
    endereco: "Av Regente Feijo, 1739 - Loja 33",
    contato: "https://api.whatsapp.com/send?phone=5511988461131&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping D",
    endereco: "Av. Cruzeiro Do Sul, 1.100 - 1º Piso - Loja 1054 - A",
    contato: "https://api.whatsapp.com/send?phone=5511988461131&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Metro Boulevard Tatuape",
    endereco: "Rua Gonçalves Crespo, 78 - 1º Piso - Quiosque 29",
    contato: "https://api.whatsapp.com/send?phone=5511988461131&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Tietê Plaza Shopping",
    endereco: "Av Raimundo Pereira De Magalhaes, 1465 - Loja 1079",
    contato: "https://api.whatsapp.com/send?phone=5511945724590&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Plaza Sul Shopping",
    endereco: "Praça Leonor Kaupa, 100 - Piso Imigrantes - Loja 181",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Jardim Sul",
    endereco: "Av. Giovanni Gronchi, 5819 - 2º Loja 417/418/419",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping SP Market",
    endereco: "Av Das Nacoes Unidas, 22.540 - Loja A6-23",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Aricanduva",
    endereco: "Av Aricanduva, 5555 - Loja 238",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Ibirapuera",
    endereco: "Av Ibirapuera, 3103 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Interlagos",
    endereco: "Av Interlagos, 2255 - Loja 259/260",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Patio Paulista",
    endereco: "R Treze De Maio, 1947 - Piso Paulista",
    contato: "https://api.whatsapp.com/send?phone=5511994561402&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Center 3",
    endereco: "Av Paulista, 2064 - Loja Luc A-41B",
    contato: "https://api.whatsapp.com/send?phone=5511965798197&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Frei Caneca",
    endereco: "Rua Frei Caneca, 569 - Loja 310",
    contato: "https://api.whatsapp.com/send?phone=5511965798197&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Center Penha",
    endereco: "R. Dr. João Ribeiro, 304 - Piso Antônio Lobo",
    contato: "https://api.whatsapp.com/send?phone=5511965798197&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Metrô Itaquera",
    endereco: "Av Jose Pinheiro Borges, S/N - Loja 158",
    contato: "https://api.whatsapp.com/send?phone=5511981152630&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Butantã Shopping",
    endereco: "Av. Prof. Francisco Morato, 2718 - Quiosque",
    contato: "https://api.whatsapp.com/send?phone=5511981152630&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "São Paulo",
    estado: "SP"
  },
  {
    nome: "Shopping Taboão",
    endereco: "Rod. Régis Bittencourt, 2643 - Loja 223",
    contato: "https://api.whatsapp.com/send?phone=5511947969812&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Taboão da Serra",
    estado: "SP"
  },
  {
    nome: "Taubate Shopping",
    endereco: "Avenida Charles Schnneider, 1.700 - 1º Piso",
    contato: "https://api.whatsapp.com/send?phone=5512997980784&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Taubate",
    estado: "SP"
  },
  {
    nome: "Via Vale Garden Shopping",
    endereco: "Avenida Dom Pedro I, 7181 - Piso L1 - Loja 20",
    contato: "https://api.whatsapp.com/send?phone=5512997140108&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Taubate",
    estado: "SP"
  },
  {
    nome: "Shopping Iguatemi Esplanada",
    endereco: "Av Gisele Constantino, 1800 - 1º Piso - Loja 164",
    contato: "https://api.whatsapp.com/send?phone=5519989882027&text=Ol%C3%A1%2C%20peguei%20seu%20contato%20pelo%20site%20e%20gostaria%20de%20informa%C3%A7%C3%B5es%20sobre%20as%20ofertas%20das%20Lojas%20Samsung.",
    horario: "Loja temporariamente fechada",
    cidade: "Votorantim",
    estado: "SP"
  },
  {
    nome: "Capim Dourado Shopping",
    endereco: "107 NORTE AV. JK, S/N - 1º Piso",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Palmas",
    estado: "TO"
  },
  {
    nome: "Palmas Shopping",
    endereco: "Quadra 101 sul, Rua NS A conjunto 2 LOTE 10 - Piso Térreo - Quiosque",
    contato: "",
    horario: "Loja temporariamente fechada",
    cidade: "Palmas",
    estado: "TO"
  }
]

//elementos selecinados

var selected_city = document.getElementById("selected-city");
var selected_state = document.getElementById("selected-state");

//inclusão de selects e manipulação de selects
//estado

estate.map(({
  nome
}) => {
  selected_state.add(new Option(nome));
});


//cidade
//--verificar a limpeza dos campos para não ter repetições
function selecionarEstado(selected_state) {

  city.map(({
    nome
  }) => {
    selected_city.remove(new Option(nome));
  });

  selected_city.add(new Option('Cidade'));

  var valor_estado = selected_state.value
  var city_per_estate = city.filter(city => city.estado == valor_estado);
  city_per_estate.map(({
    nome
  }) => {
    selected_city.add(new Option(nome));
  });
}


//filtragem de resutados
//separar lojas 
//montar elementos na tela
var divmaior = document.querySelector("#cards");

function selecionarCidade() {

  divmaior.innerHTML = "";

  var valor_estado = selected_state.value;
  var valor_cidade = selected_city.value;
  var store_per_estate = store => store.estado == valor_estado;
  var store_percity = store => store.cidade == valor_cidade;
  var store_filter = store.filter(store_per_estate).filter(store_percity);


  store_filter.map(({
    nome,
    endereco,
    contato,
    horario
  }) => {
    var divgrid = document.createElement('div');
    divgrid.classList.add('card-grid');

    var divconteudo = document.createElement('div');
    divconteudo.classList.add('card-content');

    var div_loja = document.createElement('div');
    div_loja.classList.add('estabelecimento');

    var div_endereco = document.createElement('div');
    div_endereco.classList.add('endereco');

    var div_horario = document.createElement('div');
    div_horario.classList.add('funcionamento');

    var div_link = document.createElement('div');
    div_link.classList.add('contato');
    var link = document.createElement('a');
    link.setAttribute("href", contato);
    var img = document.createElement("img");
    img.setAttribute("src", "./img/fale-com-consultor.png");

    var text_loja = document.createTextNode(nome);
    var text_endereco = document.createTextNode(endereco);
    var text_horario = document.createTextNode(horario);

    divmaior.appendChild(divgrid);
    divgrid.appendChild(divconteudo);
    divconteudo.appendChild(div_loja);
    divconteudo.appendChild(div_endereco);
    divconteudo.appendChild(div_horario);
    divconteudo.appendChild(div_link);

    div_loja.appendChild(text_loja);
    div_endereco.appendChild(text_endereco);
    div_horario.appendChild(text_horario);
    div_link.appendChild(link);
    link.appendChild(img)
    link.target = '_blank'
  });
}